public class NetworkingFeature : Feature
{
    public NetworkingFeature(
        ReplicatePawnSystem replicatePawnSystem,
        RebuildObserversSystem rebuildObserversSystem,
        ReplicatePlayerSystem replicatePlayerSystem
    ) : base("Networking")
    {
        Add(replicatePawnSystem);
        Add(replicatePlayerSystem);
        Add(rebuildObserversSystem);
    }
}