using Entitas;
using Mirror;
using UnityEngine;

public class ReplicatePawnSystem : ICleanupSystem
{
    public const float UpdateInterval = 0.05f;
    
    private Contexts _contexts;
    private IGroup<PawnEntity> _group;
    private IServerMessageService _serverMessageService;
    
    public ReplicatePawnSystem(Contexts contexts, IServerMessageService serverMessageService)
    {
        _contexts = contexts;
        _serverMessageService = serverMessageService;
        _group = _contexts.pawn.GetGroup(PawnMatcher.AllOf(PawnMatcher.Pawn, PawnMatcher.NetworkIdentity, PawnMatcher.Proximity, PawnMatcher.Replication));
    }
    
    public void Cleanup()
    {
        foreach (var pawnEntity in _group.GetEntities())
        {
            if (pawnEntity.replication.Timer >= UpdateInterval)
            {
                pawnEntity.replication.Timer = 0.0f;
                
                foreach (var entityId in pawnEntity.proximity.ObservedEntities)
                {
                    var observedPawn = _contexts.pawn.GetEntityWithNetworkIdentity(entityId);
                    if (null != observedPawn)
                    {
                        if (observedPawn.hasPlayerOwner)
                        {
                            var playerEntity = _contexts.game.GetEntityWithPlayer(observedPawn.playerOwner.PlayerId);

                            if (null != playerEntity && playerEntity.hasPlayerNetwork)
                            {
                                ReplicateToPlayer(playerEntity, pawnEntity);
                            }
                        }
                    }
                }
            }
            else
            {
                pawnEntity.replication.Timer += Time.deltaTime;
            }
        }
    }

    protected void ReplicateToPlayer(GameEntity player, PawnEntity pawnEntity)
    {
        var msg = new PawnStateMessage();
        msg.Health = pawnEntity.health.Value;
        msg.Position = pawnEntity.transform.Position;
        var velocity = pawnEntity.movement.Agent.velocity;
        msg.Velocity = new Vector2(velocity.x, velocity.z);
        msg.Rotation = pawnEntity.transform.Rotation;
        msg.PawnId = pawnEntity.pawn.Id;
        msg.Id = pawnEntity.networkIdentity.Id;
        msg.PlayerId = pawnEntity.hasPlayerOwner ? pawnEntity.playerOwner.PlayerId : -1;
        
        _serverMessageService.Send(player.playerNetwork.Connection, msg);
    }
}