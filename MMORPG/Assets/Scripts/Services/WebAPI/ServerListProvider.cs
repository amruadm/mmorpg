﻿using System.Collections;
using System.Collections.Generic;
using Mirror;
using Mirror.Websocket;
using UnityEngine;
using UnityEngine.Networking;
using Zenject;

[System.Serializable]
public class ServerInfo
{
    public string name;
    public string addr;
    public int port;

    public override string ToString()
    {
        return JsonUtility.ToJson(this);
    }
}

public delegate void ServerListLoadedHandler(ServerInfo[] servers);

public interface IServerListProvider
{
    void Load(ServerListLoadedHandler onServerListLoaded);
}

public class ServerListProvider : MonoBehaviour, IServerListProvider
{
    private event ServerListLoadedHandler _onListLoadedEvent;

    private Coroutine _loadingCoroutine;
    
    private ConfigurationProvider _configurationProvider;

    [Inject]
    public void Construct(ConfigurationProvider configurationProvider)
    {
        _configurationProvider = configurationProvider;
    }
    
    public void Load(ServerListLoadedHandler onServerListLoaded)
    {
        _onListLoadedEvent += onServerListLoaded;

        if (null == _loadingCoroutine)
        {
            _loadingCoroutine = StartCoroutine(BeginLoad());
        }
    }

    private IEnumerator BeginLoad()
    {
        string serversListUrl = _configurationProvider.ApiURL + "v1/servers/available";
        
        using (var request = UnityWebRequest.Get(serversListUrl))
        {
            yield return request.SendWebRequest();

            if (request.isNetworkError || request.isNetworkError)
            {
                Debug.Log(serversListUrl + " error code " + request.responseCode);
            }
            else
            {
                _onListLoadedEvent?.Invoke(JsonHelper.FromJsonArray<ServerInfo>(request.downloadHandler.text));
            }
        }
        
        _onListLoadedEvent = null;
        _loadingCoroutine = null;
    }
}
