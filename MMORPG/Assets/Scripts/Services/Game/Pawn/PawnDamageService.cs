using Entitas;

public class PawnDamageService : IDamageService
{
    private RandomValueProvider _randomValueProvider;
    
    public PawnDamageService(RandomValueProvider randomValueProvider)
    {
        _randomValueProvider = randomValueProvider;
    }
    
    public float CalcDamage(IEntity damageSource)
    {
        if (damageSource is PawnEntity)
        {
            var pawnEntity = (PawnEntity) damageSource;

            return _randomValueProvider.GetRange(pawnEntity.randomSeed.Value, pawnEntity.damage.Min, pawnEntity.damage.Max);
        }
        
        return 0.0f;
    }
}