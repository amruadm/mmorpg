using System.Collections.Generic;
using Entitas;
using Entitas.Unity;
using UnityEngine;

public class AttackCommandInputSystem : ReactiveSystem<InputEntity>
{
    private IClientMessageService _messageService;
    private Contexts _contexts;

    public AttackCommandInputSystem(Contexts contexts, IClientMessageService messageService) : base(contexts.input)
    {
        _messageService = messageService;
        _contexts = contexts;
    }

    protected override ICollector<InputEntity> GetTrigger(IContext<InputEntity> context)
    {
        return context.CreateCollector(InputMatcher.LeftClick);
    }

    protected override bool Filter(InputEntity entity)
    {
        return entity.hasLeftClick;
    }

    protected override void Execute(List<InputEntity> entities)
    {
        var ray = Camera.main.ScreenPointToRay(entities.SingleEntity().leftClick.ScreenPosition);
        RaycastHit hit;
        if (Physics.Raycast(ray, out hit, 10000.0f))
        {
            var entity = hit.transform.gameObject.GetEntityLink();
            if (null == entity || !(entity.entity is PawnEntity))
                return;

            var localPawn = _contexts.pawn.GetEntityWithPlayerOwner(_contexts.game.localPlayer.Owner.Id);
            if (null == localPawn)
                return;

            var pawnEntity = entity.entity as PawnEntity;

            if (localPawn.networkIdentity.Id == pawnEntity.networkIdentity.Id)
                return;
            
            _messageService.Send(new AttackCommandMessage
            {
                TargetId = (int) pawnEntity.networkIdentity.Id
            });
        }
    }
}