﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using Mirror;
using UnityEngine;
using Zenject;

public class ServerSide : MonoBehaviour
{
    private MMORPGNetworkManager _networkManager;
    
    [Inject]
    public void Construct(MMORPGNetworkManager networkManager)
    {
        _networkManager = networkManager;
    }
    
    void Start()
    {
        _networkManager.StartServer();
    }
}
