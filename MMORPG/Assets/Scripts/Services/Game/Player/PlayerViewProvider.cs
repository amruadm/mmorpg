using UnityEngine;

public interface IPlayerViewProvider
{
    GameObject PlayerPrefab { get; }
}