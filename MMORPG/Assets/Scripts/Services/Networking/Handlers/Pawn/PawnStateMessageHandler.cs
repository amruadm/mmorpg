using Mirror;
using UnityEngine;

[ClientSide]
public class PawnStateMessageHandler : MessageHandlerService<PawnStateMessage>
{
    private IPawnFactory _pawnFactory;
    private Contexts _contexts;
    
    public PawnStateMessageHandler(
        Contexts contexts,
        IClientMessageService networkMessageService,
        IPawnFactory pawnFactory
    ) : base(networkMessageService)
    {
        _contexts = contexts;
        _pawnFactory = pawnFactory;
    }

    protected override void HandleMessage(NetworkConnection connection, PawnStateMessage message)
    {
        PawnEntity pawnEntity = _contexts.pawn.GetEntityWithNetworkIdentity(message.Id);
        
        if (null == pawnEntity)
        {
            pawnEntity = _pawnFactory.CreatePawnFromId(message.PawnId);
            pawnEntity.AddNetworkIdentity(message.Id);
            pawnEntity.AddSyncTransform(message.Position, message.Rotation, Time.time);
            pawnEntity.AddMovementView(message.Velocity);
        }
        else
        {
            pawnEntity.ReplaceSyncTransform(message.Position, message.Rotation, Time.time);
            pawnEntity.ReplaceMovementView(message.Velocity);
        }

        pawnEntity.ReplaceHealth(message.Health);

        if (message.PlayerId > -1)
        {
            if (pawnEntity.hasPlayerOwner)
            {
                if (pawnEntity.playerOwner.PlayerId != message.PlayerId)
                    pawnEntity.ReplacePlayerOwner(message.PlayerId);
            }
            else
            {
                pawnEntity.AddPlayerOwner(message.PlayerId);
            }
        }
        else if (pawnEntity.hasPlayerOwner)
        {
            pawnEntity.RemovePlayerOwner();
        }
    }
}