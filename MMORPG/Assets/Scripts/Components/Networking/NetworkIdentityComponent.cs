using Entitas;
using Entitas.CodeGeneration.Attributes;

[Pawn, Game, Networking]
public class NetworkIdentityComponent : IComponent
{
    [PrimaryEntityIndex]
    public uint Id;
}