using Mirror;
using UnityEngine;

[ClientSide]
public class PlayerDataMessageHandler : MessageHandlerService<PlayerDataMessage>
{
    private Contexts _contexts;
    
    public PlayerDataMessageHandler(Contexts contexts, IClientMessageService networkMessageService) : base(networkMessageService)
    {
        _contexts = contexts;
    }

    protected override void HandleMessage(NetworkConnection connection, PlayerDataMessage message)
    {
        var localPlayerEntity = _contexts.game.GetEntityWithPlayer(message.PlayerId);
        if (null == localPlayerEntity)
        {
            Debug.Log("Create local player: " + message.PlayerId);

            localPlayerEntity = _contexts.game.CreateEntity();
            localPlayerEntity.AddPlayer(message.PlayerId, PlayerReadyState.Ready);
            localPlayerEntity.AddLocalPlayer(localPlayerEntity.player);
            localPlayerEntity.AddPlayerNetwork(connection);
        }
        else if (!localPlayerEntity.hasLocalPlayer)
        {
            Debug.Log("Add local player: " + message.PlayerId);
            localPlayerEntity.AddLocalPlayer(localPlayerEntity.player);
        }
    }
}