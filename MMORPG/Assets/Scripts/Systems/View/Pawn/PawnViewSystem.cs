using System.Collections.Generic;
using Entitas;
using UnityEngine;

public class PawnViewSystem : IExecuteSystem
{
    private const string ParamIsAttack = "IsAttack";
    private const string ParamSpeed = "Speed";
    
    private IGroup<PawnEntity> _group;
    
    public PawnViewSystem(Contexts contexts)
    {
        _group = contexts.pawn.GetGroup(PawnMatcher.AllOf(PawnMatcher.Pawn, PawnMatcher.Transform, PawnMatcher.EntityView));
    }

    public void Execute()
    {
        foreach (var pawnEntity in _group.GetEntities())
        {
            var pos = new Vector3(pawnEntity.transform.Position.x, 0.0f, pawnEntity.transform.Position.y);
            
            pawnEntity.entityView.ViewObject.transform.position = pos;
            
            pawnEntity.entityView.ViewObject.transform.rotation = Quaternion.Euler(0.0f, pawnEntity.transform.Rotation, 0.0f);
            
            var animator = pawnEntity.entityView.ViewObject.GetComponent<Animator>();
            if (animator)
            {
                if (pawnEntity.hasMovementView)
                {
                    animator.SetFloat(ParamSpeed, pawnEntity.movementView.Velocity.magnitude);                    
                }

                bool currentIsAttack = animator.GetBool(ParamIsAttack);

                if (currentIsAttack != pawnEntity.hasAttack)
                {
                    animator.SetBool(ParamIsAttack, pawnEntity.hasAttack);
                    
                    if (pawnEntity.hasAttack)
                        animator.CrossFade("Attack", 0.15f, 0);
                }
            }
        }
    }
}