using Entitas;
using Entitas.CodeGeneration.Attributes;

[Networking]
public class NetworkConnectionComponent : IComponent
{
    [PrimaryEntityIndex]
    public int ConnectionId;
}