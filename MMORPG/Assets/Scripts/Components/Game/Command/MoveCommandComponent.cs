using Entitas;
using Entitas.CodeGeneration.Attributes;
using UnityEngine;

[Command]
public class MoveCommandComponent : IComponent
{
    [PrimaryEntityIndex]
    public uint PawnId;
    public Vector2 TargetPosition;
}