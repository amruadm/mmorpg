using Mirror;

[ClientSide]
public class EntityObserveLeaveMessageHandler : MessageHandlerService<EntityObserveLeaveMessage>
{
    private Contexts _contexts;

    public EntityObserveLeaveMessageHandler(Contexts contexts, IClientMessageService networkMessageService) : base(networkMessageService)
    {
        _contexts = contexts;
    }

    protected override void HandleMessage(NetworkConnection connection, EntityObserveLeaveMessage message)
    {
        PawnEntity pawnEntity = _contexts.pawn.GetEntityWithNetworkIdentity(message.Id);
        if (null != pawnEntity)
        {
            pawnEntity.Destroy();
        }
    }
}