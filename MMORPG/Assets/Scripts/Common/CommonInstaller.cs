using System;
using System.Linq;
using System.Reflection;
using DG.Tweening;
using Entitas;
using Mirror;
using UnityEngine;
using Zenject;

/// <summary>
/// Настройка контейнера.
///
/// Примечание: Системы (ECS) должны инициализироваться после сервисов. 
/// </summary>
public class CommonInstaller : MonoInstaller<CommonInstaller>
{
    public ConfigurationProvider Configuration;
    public MMORPGNetworkManager NetworkManager;

    public PawnPrefabContainer PawnPrefabContainerAsset;
    public ScriptablePlayerViewProvider PlayerViewProviderAsset;

    public bool StartClient = true;
    public bool EnableNetwork = true;

    protected GameObject ServiceContainerSceneObject;
    protected GameObject FeautureContainerSceneObject;

    public override void InstallBindings()
    {
        CommonInstall();

#if UNITY_SERVER || UNITY_EDITOR
        ServerSideInstall();
#endif

#if !UNITY_SERVER || UNITY_EDITOR
        if (StartClient)
        {
            ClientSideInstall();
        }
#endif

        CommonSystemsInstall();

#if UNITY_SERVER || UNITY_EDITOR
        ServerSideSystemsInstall();
#endif

#if !UNITY_SERVER || UNITY_EDITOR
        if (StartClient)
        {
            ClientSideSystemsInstall();
        }
#endif

        FeautureContainerSceneObject = new GameObject("Feauture Controller");
        
        CommonFeautureInstall();

#if UNITY_SERVER || UNITY_EDITOR
        ServerSideFeautureInstall();
#endif

#if !UNITY_SERVER || UNITY_EDITOR
        if (StartClient)
        {
            ClientSideFeautureInstall();
        }
#endif
        
        Container.Bind<FeautureController>().FromNewComponentOn(FeautureContainerSceneObject).AsSingle().NonLazy();

        AfterInstall();
    }

    #region Services Install

    void CommonInstall()
    {
        Container.BindInstances(Contexts.sharedInstance);

        Container.Bind<Transport>().FromInstance(GetComponent<Transport>()).AsSingle();

        ServiceContainerSceneObject = new GameObject("Service Container");

        Container.BindInstance(Configuration);

        Container.Bind<MMORPGNetworkManager>().FromNewComponentOn(ServiceContainerSceneObject).AsSingle().NonLazy();
        
        #if UNITY_EDITOR
        Container
            .Bind<INetworkSideContext>()
            .To<EditorNetworkSideContext>()
            .FromInstance(new EditorNetworkSideContext(EnableNetwork))
            .AsSingle()
        ;
        #endif

        Container.Bind<RandomValueProvider>().AsSingle();
        Container.Bind<IDamageService>().To<PawnDamageService>().AsSingle();
        Container.Bind<IPawnFactory>().To<PawnFromPrefabFactory>().AsSingle();
        Container.Bind<IPawnDataProvider>().To<PawnPrefabContainer>().FromInstance(PawnPrefabContainerAsset).AsSingle();
        Container.Bind<IPawnAttributeValueProvider>().To<TestAttributeValueProvider>().AsSingle();
        if (!PlayerViewProviderAsset)
            Debug.LogError("Player view provider missing reference!");
        Container.Bind<IPlayerViewProvider>().To<ScriptablePlayerViewProvider>().FromInstance(PlayerViewProviderAsset).AsSingle();
        Container.Bind<SpawnPointProvider>().AsSingle().NonLazy();
    }

    void ServerSideInstall()
    {
        #if !UNITY_EDITOR
        Container.Bind<INetworkSideContext>().To<ServerNetworkSideContext>().AsSingle().NonLazy();
        #endif
        
        Container.Bind<IServerMessageService>().To<ServerMessageService>().AsSingle().NonLazy();
        
        Container.Bind<WebAuthService>().FromNewComponentOn(ServiceContainerSceneObject).AsSingle().NonLazy();
        Container.Bind<SpawnerInitializer>().FromNewComponentOn(ServiceContainerSceneObject).AsSingle().NonLazy();

        Container.Bind<ServerSide>().FromNewComponentOn(new GameObject("Server Side")).AsSingle().NonLazy();

        var hsndlers = ReflectionHelper.GetImplementations(typeof(IMessageHandlerService));
        foreach (var handlerClass in hsndlers)
        {
            var attr = handlerClass.GetCustomAttribute(typeof(ServerSideAttribute));
            if (null != attr)
            {
                Container.Bind<IMessageHandlerService>().To(handlerClass).AsSingle().NonLazy();
            }
        }
    }

    void ClientSideInstall()
    {
        DOTween.Init();
        
        #if !UNITY_EDITOR
        Container.Bind<INetworkSideContext>().To<ClientNetworkSideContext>().AsSingle().NonLazy();
        #endif

        Container.Bind<IClientMessageService>().To<ClientMessageService>().AsSingle().NonLazy();

        Container.Bind<SimpleUserCreditinalsProvider>().AsSingle().NonLazy();
        
        #if UNITY_EDITOR
        Container.Bind<IServerListProvider>().To<LocalhostServerListProvider>().AsSingle().NonLazy();
        #else
        Container.Bind<IServerListProvider>().To<ServerListProvider>().FromNewComponentOn(ServiceContainerSceneObject).AsSingle().NonLazy();
        #endif

        Container.Bind<ClientSide>().FromNewComponentOn(new GameObject("Client Side")).AsSingle().NonLazy();
        
        var hsndlers = ReflectionHelper.GetImplementations(typeof(IMessageHandlerService));
        foreach (var handlerClass in hsndlers)
        {
            var attr = handlerClass.GetCustomAttribute(typeof(ClientSideAttribute));
            if (null != attr)
            {
                Container.Bind<IMessageHandlerService>().To(handlerClass).AsSingle().NonLazy();
            }
        }
    }

    #endregion

    #region Systems Install

    void ClientSideSystemsInstall()
    {
    }

    void ServerSideSystemsInstall()
    {
        
    }

    void CommonSystemsInstall()
    {
        foreach (var implementation in ReflectionHelper.GetImplementations(typeof(ISystem)))
        {
            if (implementation != typeof(Systems) && !implementation.IsSubclassOf(typeof(Systems)))
            {
                Container.Bind(implementation).AsSingle();
            }
        }
    }

    #endregion

    #region Feauture Install

    void CommonFeautureInstall()
    {
        
    }

    void ClientSideFeautureInstall()
    {
        Container.Bind<Feature>().To<InputFeature>().AsSingle().NonLazy();
        Container.Bind<Feature>().To<ClientPawnFeature>().AsSingle().NonLazy();
        Container.Bind<Feature>().To<ViewFeature>().AsSingle().NonLazy();
    }

    void ServerSideFeautureInstall()
    {
        Container.Bind<Feature>().To<ServerGameFeature>().AsSingle().NonLazy();
        Container.Bind<Feature>().To<ServerPawnFeature>().AsSingle().NonLazy();
        Container.Bind<Feature>().To<CommandFeature>().AsSingle().NonLazy();
        Container.Bind<Feature>().To<NetworkingFeature>().AsSingle().NonLazy();
    }

    #endregion
    
    #region After install

    void AfterInstall()
    {
        // todo !!!
//        Container.Bind<IPlayerLoginService>().To<SinglePlayerLoginService>().FromNewComponentOn(ServiceContainerSceneObject).AsSingle().NonLazy();
#if UNITY_SERVER || UNITY_EDITOR
        Container.Bind<IPlayerLoginService>().To<SimpleLoginService>().AsSingle().NonLazy();
#endif
    }
    #endregion
}