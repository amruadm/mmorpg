using Entitas;
using UnityEngine;

[Pawn, Networking]
public class SyncTransformComponent : IComponent
{
    public Vector2 Position;
    public float Rotation;
    public float LastSyncTime;
}