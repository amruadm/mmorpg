using System.IO;
using System.Linq;
using DesperateDevs.CodeGeneration;
using Entitas.CodeGeneration.Plugins;

namespace CodeGeneration.Plugins
{
    public class PlacedEntityGenerator : ICodeGenerator
    {
        public string name => "PlacedEntity";
        public int priority => 0;
        public bool runInDryMode => true;

        private const string DirectoryName = "Behaviors";

        private const string flagTemplate = 
@"using UnityEngine;
using Entitas;

public class ${ClassName} : MonoBehaviour
{
    public void Initialize(${Context}Entity entity)
    {
        entity.is{ComponentName} = true;
    }
}
";
        
        private const string StandartTemplate = 
@"using UnityEngine;
using Entitas;

public class ${ClassName} : MonoBehaviour
{
${Fields}

    public void Initialize(${Context}Entity entity)
    {
        entity.Replace${ComponentName}(${Args});
    }
}
";

        private const string FieldTemplate = "    public ${FieldType} ${FieldName};";
        private const string ArgTemplate = "${ArgName}";
        
        public CodeGenFile[] Generate(CodeGeneratorData[] data)
        {
            return data.OfType<PlacedEntityData>()
                .Select(Generate)
                .ToArray()
            ;
        }

        private CodeGenFile Generate(PlacedEntityData data)
        {
            var componentName = data.Name.ToComponentName(true);
            var className = componentName + "Script";
            var filename = DirectoryName + Path.DirectorySeparatorChar + className + ".cs";
            var memberData = data.MemberData;
            var context = data.ContextAttribute.contextName;

            var template = 0 == memberData.Length ? flagTemplate : StandartTemplate;

            var fileContent = template
                .Replace("${ClassName}", className)
                .Replace("${ComponentName}", componentName)
                .Replace("${Context}", context)
                .Replace("${Fields}", GenerateFields(memberData))
                .Replace("${Args}", GemerateArgs(memberData))
            ;
            
            return new CodeGenFile(filename, fileContent, GetType().FullName);
        }

        private string GenerateFields(MemberData[] memberData)
        {
            return string.Join("\n", memberData
                .Select(member => FieldTemplate
                    .Replace("${FieldType}", member.type)
                    .Replace("${FieldName}", member.name)
                )
            );
        }

        private string GemerateArgs(MemberData[] memberData)
        {
            return string.Join(", ", memberData
                .Select(member => ArgTemplate
                    .Replace("${ArgName}", member.name)
                )
            );
        }
    }
}