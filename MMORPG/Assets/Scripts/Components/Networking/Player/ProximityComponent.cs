using System.Collections.Generic;
using Entitas;

[Pawn, Game]
public class ProximityComponent : IComponent
{
    public float LastUpdateTime;
    public HashSet<uint> ObservedEntities;
}