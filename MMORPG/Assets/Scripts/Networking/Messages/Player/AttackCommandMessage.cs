using Mirror;

public class AttackCommandMessage : MessageBase
{
    public int TargetId;

    public override void Serialize(NetworkWriter writer)
    {
        writer.Write(TargetId);
    }

    public override void Deserialize(NetworkReader reader)
    {
        TargetId = reader.ReadInt32();
    }
}