using Entitas;
using UnityEngine;

public class PlayerViewSystem : IExecuteSystem
{
    private Contexts _contexts;
    
    public PlayerViewSystem(Contexts contexts)
    {
        _contexts = contexts;
    }
    
    public void Execute()
    {
        if (_contexts.game.hasPlayerView && _contexts.game.hasLocalPlayer)
        {
            var pawn = _contexts.pawn.GetEntityWithPlayerOwner(_contexts.game.localPlayer.Owner.Id);
            if (null != pawn)
            {
                var pos2d = pawn.transform.Position;
                var target = new Vector3(pos2d.x, 0.0f, pos2d.y);

                var pos = _contexts.game.playerView.ViewObject.transform.position;

                var smoothPos = Vector3.MoveTowards(pos, target, Vector3.Distance(pos, target) * Time.deltaTime * 10.0f);

                _contexts.game.playerView.ViewObject.transform.position = smoothPos;                
            }
        }
    }
}