using System;
using System.Collections.Generic;
using UnityEngine;

[Serializable]
public class PawnPrefabContainerItem : IPawnData
{
    [SerializeField]
    private int _uniqueId;
    
    [SerializeField]
    private GameObject _prefab;
    
    [SerializeField]
    private GameObject _viewPrefab;

    public int Id => _uniqueId;
    public GameObject Prefab => _prefab;
    public GameObject ViewPrefab => _viewPrefab;
}

[CreateAssetMenu(menuName = "Game/Pawn Prefab Container")]
public class PawnPrefabContainer : ScriptableObject, IPawnDataProvider
{
    [SerializeField]
    private PawnPrefabContainerItem[] _prefabs;
    
    public IDictionary<int, IPawnData> GetAllData()
    {
        var dictionary = new Dictionary<int, IPawnData>();
        
        foreach (var item in _prefabs)
        {
            dictionary.Add(item.Id, item);
        }

        return dictionary;
    }
}