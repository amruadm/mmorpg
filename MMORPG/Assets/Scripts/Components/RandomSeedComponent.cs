using Entitas;

[Game, Pawn]
public class RandomSeedComponent : IComponent
{
    public int Value;
}