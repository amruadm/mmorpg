using System;
using Mirror;

public interface INetworkMessageService
{
    void RegisterHandler<T>(Action<NetworkConnection, T> handler) where T : IMessageBase, new();
    void UnregisterHandler<T>() where T : IMessageBase;
}