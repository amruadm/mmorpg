using System;

[Serializable]
public struct DamageRange
{
    public float Min;
    public float Max;
}

public interface IPawnAttributeValueProvider
{
    DamageRange GetDamageValue(PawnEntity pawnEntity);
    float GetAttackSpeed(PawnEntity pawnEntity);
    float GetAttackActionTimer(PawnEntity pawnEntity);
    float GetAttackDistance(PawnEntity pawnEntity);
}