using System;
using DesperateDevs.CodeGeneration;
using Entitas.CodeGeneration.Attributes;
using Entitas.CodeGeneration.Plugins;

namespace CodeGeneration.Plugins
{
    public class PlacedEntityData : CodeGeneratorData
    {
        public const string NameKey = "PlacedEntity.Name";
        public const string DataKey = "PlacedEntity.Data";
        public const string ContextKey = "PlacedEntity.Context";

        public string Name
        {
            get => (string) this[NameKey];
            set => this[NameKey] = value;
        }

        public MemberData[] MemberData
        {
            get => (MemberData[])this[DataKey];
            set => this[DataKey] = value;
        }

        public ContextAttribute ContextAttribute
        {
            get => (ContextAttribute) this[ContextKey];
            set => this[ContextKey] = value;
        }
    }
}