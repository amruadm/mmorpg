using System.Collections.Generic;
using Entitas;

public class PlayerSpawnSystem : ReactiveSystem<GameEntity>
{
    // todo !!!
    public const int PlayerPawnPrefabId = 1;

    private Contexts _contexts;
    private IPawnFactory _pawnFactory;
    private IPawnDataProvider _pawnDataProvider;
    private SpawnPointProvider _spawnPointProvider;

    public PlayerSpawnSystem(
        Contexts contexts,
        IPawnFactory pawnFactory,
        IPawnDataProvider pawnDataProvider,
        SpawnPointProvider spawnPointProvider
    ) : base(contexts.game)
    {
        _contexts = contexts;
        _pawnFactory = pawnFactory;
        _pawnDataProvider = pawnDataProvider;
        _spawnPointProvider = spawnPointProvider;
    }

    protected override ICollector<GameEntity> GetTrigger(IContext<GameEntity> context)
    {
        return context.CreateCollector(GameMatcher.Player);
    }

    protected override bool Filter(GameEntity entity)
    {
        return entity.hasPlayer && entity.player.State == PlayerReadyState.Ready;
    }

    protected override void Execute(List<GameEntity> entities)
    {
        foreach (var gameEntity in entities)
        {
            
            var pawnEntity = _contexts.pawn.GetEntityWithPlayerOwner(gameEntity.player.Id);
            if (null == pawnEntity)
            {
                pawnEntity = _pawnFactory.CreatePawnFromData(_pawnDataProvider.GetAllData()[PlayerPawnPrefabId]);

                var spawnPoint = _spawnPointProvider.GetSpawnPoint();
                if (spawnPoint)
                {
                    pawnEntity.movement.Agent.transform.position = spawnPoint.transform.position;                    
                }
            }

            if (!pawnEntity.hasPlayerOwner)
                pawnEntity.AddPlayerOwner(gameEntity.player.Id);
        }
    }
}