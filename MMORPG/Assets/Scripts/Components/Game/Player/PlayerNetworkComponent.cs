using Entitas;
using Entitas.CodeGeneration.Attributes;
using Mirror;

[Game]
public class PlayerNetworkComponent : IComponent
{
    [PrimaryEntityIndex]
    public NetworkConnection Connection;
}