using Entitas;
using Entitas.CodeGeneration.Attributes;

public enum PlayerReadyState
{
    NotReady,
    Ready
}

[Game]
[Event(EventTarget.Any)]
[Event(EventTarget.Any, EventType.Removed)]
public class PlayerComponent : IComponent
{
    [PrimaryEntityIndex]
    public int Id;
    public PlayerReadyState State = PlayerReadyState.NotReady;
}