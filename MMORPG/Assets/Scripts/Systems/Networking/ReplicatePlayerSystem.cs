using Entitas;
using UnityEngine;

public class ReplicatePlayerSystem : ICleanupSystem
{
    private const float UpdateInterval = 1.0f;
    
    private IGroup<GameEntity> _group;
    private IServerMessageService _serverMessageService;
    
    public ReplicatePlayerSystem(Contexts contexts, IServerMessageService serverMessageService)
    {
        _serverMessageService = serverMessageService;
        _group = contexts.game.GetGroup(GameMatcher.Player);
    }

    public void Cleanup()
    {
        foreach (var gameEntity in _group.GetEntities())
        {
            if (gameEntity.replication.Timer >= UpdateInterval)
            {
                gameEntity.replication.Timer = 0.0f;
                
                Replicate(gameEntity);
            }
            else
            {
                gameEntity.replication.Timer += Time.deltaTime;
            }
        }
    }

    private void Replicate(GameEntity playerEntity)
    {
        var msg = new PlayerDataMessage
        {
            PlayerId = playerEntity.player.Id
        };
        
        _serverMessageService.Send(playerEntity.playerNetwork.Connection, msg);
    }
}