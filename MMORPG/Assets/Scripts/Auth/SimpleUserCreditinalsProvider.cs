﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[Serializable]
public class UserCreditinalsData
{
    [SerializeField]
    private string login;
    
    [SerializeField]
    private string password;

    public string Login
    {
        get { return login; }
        set { login = value; }
    }
    public string Password
    {
        get { return password; }
        set { password = value; }
    }
}

public class SimpleUserCreditinalsProvider
{
    public UserCreditinalsData Creditinals { get; set; }

    public SimpleUserCreditinalsProvider()
    {
        Creditinals = new UserCreditinalsData();
    }
}
