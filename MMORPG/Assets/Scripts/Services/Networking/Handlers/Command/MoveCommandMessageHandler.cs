using Mirror;
using UnityEngine;

[ServerSide]
public class MoveCommandMessageHandler : MessageHandlerService<MoveCommandMessage>
{
    private Contexts _contexts;

    public MoveCommandMessageHandler(
        Contexts contexts,
        IServerMessageService networkMessageService
    ) : base(networkMessageService)
    {
        _contexts = contexts;
    }

    protected override void HandleMessage(NetworkConnection connection, MoveCommandMessage message)
    {
        var playerEntity = _contexts.game.GetEntityWithPlayerNetwork(connection);
        if (null == playerEntity) return;

        var pawnEntity = _contexts.pawn.GetEntityWithPlayerOwner(playerEntity.player.Id);
        if (null == pawnEntity) return;
        
        CommandHelper.ClearCommands(_contexts.command, pawnEntity.networkIdentity.Id);

        var cmd = _contexts.command.CreateEntity();
        cmd.AddMoveCommand(pawnEntity.networkIdentity.Id, message.TargetPosition);
    }
}