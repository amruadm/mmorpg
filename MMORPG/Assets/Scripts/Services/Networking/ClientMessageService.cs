using System;
using Mirror;
using UnityEngine;

public interface IClientMessageService : INetworkMessageService
{
    void Send<T>(T message) where T : IMessageBase, new();
}

public class ClientMessageService : IClientMessageService
{
    public void RegisterHandler<T>(Action<NetworkConnection, T> handler) where T : IMessageBase, new()
    {
        Debug.Log("Register client handler: " + MessagePacker.GetId<T>());
        NetworkClient.RegisterHandler(handler);
    }

    public void UnregisterHandler<T>() where T : IMessageBase
    {
        NetworkClient.UnregisterHandler<T>();
    }

    public void Send<T>(T message) where T : IMessageBase, new()
    {
        NetworkClient.Send(message);
    }
}