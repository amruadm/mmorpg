public class ClientPawnFeature : Feature
{
    public ClientPawnFeature(
        #if !UNITY_EDITOR
        SyncTransformSystem syncTransformSystem
        #endif
    ) : base("Pawn")
    {
        #if !UNITY_EDITOR
        Add(syncTransformSystem);
        #endif
    }
}