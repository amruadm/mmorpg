using Entitas;
using UnityEngine;

public interface IPawnData
{
    int Id { get; }
    GameObject Prefab { get; }
    GameObject ViewPrefab { get; }
}

public interface IPawnFactory
{
    PawnEntity CreateEmptyPawn();
    PawnEntity CreatePawnFromData(IPawnData pawnData);
    PawnEntity CreatePawnFromId(int id);
    GameObject CreatePawnViewFromData(IPawnData pawnData);
}