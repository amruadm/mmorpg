public class ClientNetworkSideContext : INetworkSideContext
{
    public bool IsServer => false;
    public bool IsClient => true;
    public bool IsEnabled => true;
}