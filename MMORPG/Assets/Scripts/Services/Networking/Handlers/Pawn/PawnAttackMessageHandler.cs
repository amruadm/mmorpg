using Mirror;

[ClientSide]
public class PawnAttackMessageHandler : MessageHandlerService<PawnAttackMessage>
{
    private Contexts _contexts;
    
    public PawnAttackMessageHandler(Contexts contexts, IClientMessageService networkMessageService) : base(networkMessageService)
    {
        _contexts = contexts;
    }

    protected override void HandleMessage(NetworkConnection connection, PawnAttackMessage message)
    {
        
    }
}