﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.Networking;
using Zenject;

[Serializable]
public class UserInfo
{
    public int id;
    public string login;
    public string email;
}

public class CreditinalsCheckQueueItem
{
    public UserCreditinalsData Creaditinals; 
    public WebAuthService.CreditinalsCheckCompletionHandler Handler;
}

public class WebAuthService : MonoBehaviour, IAuthService
{
    public delegate void CreditinalsCheckCompletionHandler(UserInfo info, bool hasSuccess);
    
    private Coroutine _loadingCoroutine;
    
    private ConfigurationProvider _configurationProvider;

    private Queue<CreditinalsCheckQueueItem> _items = new Queue<CreditinalsCheckQueueItem>();

    [Inject]
    public void Construct(ConfigurationProvider configurationProvider)
    {
        _configurationProvider = configurationProvider;
    }
    
    public void CheckCreditinals(UserCreditinalsData creditinals, CreditinalsCheckCompletionHandler onCreditinalsCheckComplete)
    {
        _items.Enqueue(new CreditinalsCheckQueueItem
        {
            Creaditinals = creditinals,
            Handler = onCreditinalsCheckComplete
        });
        
        if (null == _loadingCoroutine)
        {
            _loadingCoroutine = StartCoroutine(ProcessQueue());
        }
    }

    private IEnumerator ProcessQueue()
    {
        string serversListUrl = _configurationProvider.ApiURL + "v1/user/check-auth";

        while (_items.Count > 0)
        {
            var item = _items.Dequeue();
            
            using (var request = UnityWebRequest.Post(serversListUrl, JsonUtility.ToJson(item.Creaditinals)))
            {
                yield return request.SendWebRequest();
                if (request.isNetworkError)
                {
                    Debug.Log(serversListUrl + " error code " + request.responseCode);
                }
                else
                {
                    if (false == request.isHttpError)
                    {
                        item.Handler?.Invoke(JsonUtility.FromJson<UserInfo>(request.downloadHandler.text), true);
                    }
                    else
                    {
                        item.Handler?.Invoke(null, false);
                    }
                }
            }
        }

        _loadingCoroutine = null;
    }
}
