using Entitas;
using UnityEngine;

[Pawn, Game]
public class MovementViewComponent : IComponent
{
    public Vector2 Velocity;
}