using Entitas;
using Entitas.CodeGeneration.Attributes;

[Pawn]
public class PlayerOwnerComponent : IComponent
{
    [PrimaryEntityIndex]
    public int PlayerId;
}