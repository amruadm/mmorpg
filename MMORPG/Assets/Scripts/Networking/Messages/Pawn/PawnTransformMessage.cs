using Mirror;
using UnityEngine;

public class PawnTransformMessage : IdentityMessage
{
    public Vector2 Position;
    public Vector2 Velocity;
    public float Rotation;

    public override void Serialize(NetworkWriter writer)
    {
        base.Serialize(writer);
        
        writer.Write(Position);
        writer.Write(Velocity);
        writer.Write(Rotation);
    }

    public override void Deserialize(NetworkReader reader)
    {
        base.Deserialize(reader);

        Position = reader.ReadVector2();
        Velocity = reader.ReadVector2();
        Rotation = reader.ReadSingle();
    }
}