using Entitas;
using UnityEngine;

public class PawnSpawnerSystem : ICleanupSystem
{
    private IPawnFactory _pawnFactory;
    private IGroup<EnvironmentEntity> _group;
    
    public PawnSpawnerSystem(Contexts contexts, IPawnFactory pawnFactory)
    {
        _pawnFactory = pawnFactory;
        _group = contexts.environment.GetGroup(EnvironmentMatcher.PawnSpawner);
    }
    
    public void Cleanup()
    {
        foreach (var spawnerEntity in _group.GetEntities())
        {
            var pawnEntity = _pawnFactory.CreatePawnFromId(spawnerEntity.pawnSpawner.PawnId);

            pawnEntity.movement.Agent.transform.position = spawnerEntity.gameObjectLink.Value.transform.position;
            
            spawnerEntity.Destroy();
        }
    }
}