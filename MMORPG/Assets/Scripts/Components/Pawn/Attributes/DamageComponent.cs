using Entitas;

[Pawn]
public class DamageComponent : IComponent, IAttribute
{
    public float Min;
    public float Max;
}