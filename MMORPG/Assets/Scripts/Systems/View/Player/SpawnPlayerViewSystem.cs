using System.Collections.Generic;
using Entitas;
using UnityEngine;

public class SpawnPlayerViewSystem : ReactiveSystem<GameEntity>
{
    private IPlayerViewProvider _playerViewProvider;
    private Contexts _contexts;
    
    public SpawnPlayerViewSystem(Contexts contexts, IPlayerViewProvider playerViewProvider): base(contexts.game)
    {
        _playerViewProvider = playerViewProvider;
        _contexts = contexts;
    }

    protected override ICollector<GameEntity> GetTrigger(IContext<GameEntity> context)
    {
        return context.CreateCollector(GameMatcher.LocalPlayer.Added());
    }

    protected override bool Filter(GameEntity entity)
    {
        return entity.hasLocalPlayer;
    }

    protected override void Execute(List<GameEntity> entities)
    {
        Debug.Log("Spawn player view");
        
        var playerEntity = entities.SingleEntity();

        if (null == _playerViewProvider)
        {
            Debug.LogError("Player view provider not initialized!");

            return;
        }
        
        var playerView = GameObject.Instantiate(_playerViewProvider.PlayerPrefab);
        
        playerEntity.AddPlayerView(playerView);
    }
}