public static class CommandHelper
{
    public static void ClearCommands(CommandContext commandContext, uint pawnId)
    {
        var moveCommandEntity = commandContext.GetEntityWithMoveCommand(pawnId);
        if (null != moveCommandEntity)
            moveCommandEntity.Destroy();
        
        var attackCommandEntity = commandContext.GetEntityWithAttackCommand(pawnId);
        if (null != attackCommandEntity)
            attackCommandEntity.Destroy();
    }
}