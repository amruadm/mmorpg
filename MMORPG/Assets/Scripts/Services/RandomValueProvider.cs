using UnityEngine;

public class RandomValueProvider
{
    public int GetRange(int seed, int min, int max)
    {
        Random.InitState(seed);
        
        return Random.Range(min, max);
    }

    public float GetRange(int seed, float min, float max)
    {
        Random.InitState(seed);
        
        return Random.Range(min, max);
    }

    public bool Chance(int seed, float chance)
    {
        return GetRange(seed, 0.0f, 1.0f) > chance;
    }

    public int RandomSeed => Random.Range(0, int.MaxValue / 2);
}