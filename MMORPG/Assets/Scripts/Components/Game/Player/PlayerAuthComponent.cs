using Entitas;

public enum PlayerState
{
    NonAuthenticated,
    WaitForAuthenticate,
    Authenticated
}

[Game]
public class PlayerAuthComponent : IComponent
{
    public PlayerState State;
    public float AuthTimeout;
}