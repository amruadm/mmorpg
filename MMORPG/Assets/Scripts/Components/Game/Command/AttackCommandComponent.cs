using Entitas;
using Entitas.CodeGeneration.Attributes;

[Command]
public class AttackCommandComponent : IComponent
{
    [PrimaryEntityIndex]
    public uint InstigatorPawn;
    public uint TargetPawn;
}