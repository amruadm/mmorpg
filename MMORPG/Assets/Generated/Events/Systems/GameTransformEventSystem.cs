//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by Entitas.CodeGeneration.Plugins.EventSystemGenerator.
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------
public sealed class GameTransformEventSystem : Entitas.ReactiveSystem<GameEntity> {

    readonly System.Collections.Generic.List<IGameTransformListener> _listenerBuffer;

    public GameTransformEventSystem(Contexts contexts) : base(contexts.game) {
        _listenerBuffer = new System.Collections.Generic.List<IGameTransformListener>();
    }

    protected override Entitas.ICollector<GameEntity> GetTrigger(Entitas.IContext<GameEntity> context) {
        return Entitas.CollectorContextExtension.CreateCollector(
            context, Entitas.TriggerOnEventMatcherExtension.Added(GameMatcher.Transform)
        );
    }

    protected override bool Filter(GameEntity entity) {
        return entity.hasTransform && entity.hasGameTransformListener;
    }

    protected override void Execute(System.Collections.Generic.List<GameEntity> entities) {
        foreach (var e in entities) {
            var component = e.transform;
            _listenerBuffer.Clear();
            _listenerBuffer.AddRange(e.gameTransformListener.value);
            foreach (var listener in _listenerBuffer) {
                listener.OnTransform(e, component.Position, component.Rotation);
            }
        }
    }
}
