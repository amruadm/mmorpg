using System.Collections.Generic;

public interface IPawnDataProvider
{
    IDictionary<int, IPawnData> GetAllData();
}