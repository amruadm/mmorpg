public class ServerNetworkSideContext : INetworkSideContext
{
    public bool IsServer => true;
    public bool IsClient => false;
    public bool IsEnabled => true;
}