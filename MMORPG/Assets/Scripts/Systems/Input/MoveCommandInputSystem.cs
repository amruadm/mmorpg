using System.Collections.Generic;
using Entitas;
using UnityEngine;

public class MoveCommandInputSystem : ReactiveSystem<InputEntity>
{
    private Contexts _contexts;
    private IClientMessageService _messageService;
    
    public MoveCommandInputSystem(Contexts contexts, IClientMessageService messageService) : base(contexts.input)
    {
        _contexts = contexts;
        _messageService = messageService;
    }

    protected override ICollector<InputEntity> GetTrigger(IContext<InputEntity> context)
    {
        return context.CreateCollector(InputMatcher.LeftClick);
    }

    protected override bool Filter(InputEntity entity)
    {
        return entity.hasLeftClick;
    }

    protected override void Execute(List<InputEntity> entities)
    {
        var ray = Camera.main.ScreenPointToRay(entities.SingleEntity().leftClick.ScreenPosition);
        RaycastHit hit;
        if (Physics.Raycast(ray, out hit, 10000.0f))
        {
            var hitPoint = new Vector2(hit.point.x, hit.point.z);
            
            _messageService.Send(new MoveCommandMessage
            {
                TargetPosition = hitPoint
            });
        }
    }
}