public class TestAttributeValueProvider : IPawnAttributeValueProvider
{
    public DamageRange GetDamageValue(PawnEntity pawnEntity)
    {
        return new DamageRange
        {
            Min = 5,
            Max = 10,
        };
    }

    public float GetAttackSpeed(PawnEntity pawnEntity)
    {
        return 2.0f;
    }

    public float GetAttackActionTimer(PawnEntity pawnEntity)
    {
        return 0.5f;
    }

    public float GetAttackDistance(PawnEntity pawnEntity)
    {
        return 1.0f;
    }
}