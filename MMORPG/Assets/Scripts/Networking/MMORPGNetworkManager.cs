﻿using System;
using System.Collections;
using System.Collections.Generic;
using Mirror;
using Mirror.Websocket;
using UnityEngine;
using Zenject;

public class MMORPGNetworkManager : MonoBehaviour
{
    public string ServerAddr = "127.0.0.1";
    public int ServerPort = 7777;
    
    private Transport _transport;

    [Inject]
    public void Construct(Transport transport)
    {
        _transport = transport;
        
        Transport.activeTransport = _transport;
    }

    public void StartClient()
    {
        if (_transport is WebsocketTransport)
        {
            var wsTransport = (WebsocketTransport) _transport;
            wsTransport.port = ServerPort;
        }
        
        NetworkClient.Connect(ServerAddr);
    }

    public void StartServer()
    {
        NetworkServer.Listen(10000);
    }
}
