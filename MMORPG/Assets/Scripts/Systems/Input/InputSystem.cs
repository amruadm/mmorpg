using Entitas;
using UnityEngine;

public class InputSystem : IExecuteSystem
{
    private const int LeftMouseButton = 0;
    private const int RightMouseButton = 1;
    
    private Contexts _contexts;
    
    public InputSystem(Contexts contexts)
    {
        _contexts = contexts;
    }

    public void Execute()
    {
        if (Input.GetMouseButtonDown(LeftMouseButton))
        {
            _contexts.input.ReplaceLeftClick(Input.mousePosition);
        }

        if (Input.GetMouseButtonDown(LeftMouseButton))
        {
            // todo !!! right click
            _contexts.input.ReplaceLeftClick(Input.mousePosition);
        }
    }
}