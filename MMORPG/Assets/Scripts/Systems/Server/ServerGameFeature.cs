using System.Collections.Generic;
using Entitas;

public class ServerGameFeature : Feature
{
    public ServerGameFeature(
        PlayerSpawnSystem playerSpawnSystem,
        DamageSystem damageSystem,
        PawnSpawnerSystem pawnSpawnerSystem
    ) : base("Game")
    {
        Add(playerSpawnSystem);
        Add(damageSystem);
        Add(pawnSpawnerSystem);
    }
}