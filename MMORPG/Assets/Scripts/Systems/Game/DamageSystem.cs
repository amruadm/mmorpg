using Entitas;
using UnityEngine;

public class DamageSystem : IExecuteSystem, ICleanupSystem
{
    private Contexts _contexts;
    private IGroup<GameEntity> _damageEventGroup;
    
    public DamageSystem(Contexts contexts)
    {
        _contexts = contexts;

        _damageEventGroup = _contexts.game.GetGroup(GameMatcher.DamageEvent);
    }
    
    public void Execute()
    {
        foreach (var damageEventEntity in _damageEventGroup.GetEntities())
        {
            float health = damageEventEntity.damageEvent.Target.health.Value;
            health = Mathf.Max(health - damageEventEntity.damageEvent.Damage, 0.0f);
            damageEventEntity.damageEvent.Target.ReplaceHealth(health);

            if (health <= 0.0f)
            {
                damageEventEntity.damageEvent.Target.isDie = true;
            }
        }
    }

    public void Cleanup()
    {
        foreach (var damageEventEntity in _damageEventGroup.GetEntities())
        {
            damageEventEntity.Destroy();
        }
    }
}