using Entitas;
using UnityEngine;

public class AttackSystem : ICleanupSystem
{
    private Contexts _contexts;
    private IGroup<PawnEntity> _group;
    private IPawnAttributeValueProvider _pawnAttributeValueProvider;
    private RandomValueProvider _randomValueProvider;

    public AttackSystem(
        Contexts contexts,
        IPawnAttributeValueProvider pawnAttributeValueProvider,
        RandomValueProvider randomValueProvider
    )
    {
        _contexts = contexts;
        _group = _contexts.pawn.GetGroup(PawnMatcher.Attack);
        _pawnAttributeValueProvider = pawnAttributeValueProvider;
        _randomValueProvider = randomValueProvider;
    }

    public void Cleanup()
    {
        foreach (var pawnEntity in _group.GetEntities())
        {
            var attackSpeed = _pawnAttributeValueProvider.GetAttackSpeed(pawnEntity);
            var attackActionTimer = _pawnAttributeValueProvider.GetAttackSpeed(pawnEntity);

            if (pawnEntity.attack.Timer >= 1.0f / attackSpeed)
            {
                Debug.Log("RemoveAttack");
                pawnEntity.RemoveAttack();

                continue;
            }

            if (!pawnEntity.attack.Triggered && pawnEntity.attack.Timer >= attackActionTimer)
            {
                pawnEntity.attack.Triggered = true;
                
                var damageEventEntity = _contexts.game.CreateEntity();
                var target = _contexts.pawn.GetEntityWithNetworkIdentity((uint) pawnEntity.attack.TargetId);
                if (null != target)
                {
                    var damageRange = _pawnAttributeValueProvider.GetDamageValue(pawnEntity);
                    
                    pawnEntity.ReplaceRandomSeed(_randomValueProvider.RandomSeed);
                    var damage = _randomValueProvider.GetRange(pawnEntity.randomSeed.Value, damageRange.Min, damageRange.Max);

                    damageEventEntity.AddDamageEvent(pawnEntity, target, damage);
                }
            }
            
            pawnEntity.attack.Timer += Time.deltaTime;
        }
    }
}