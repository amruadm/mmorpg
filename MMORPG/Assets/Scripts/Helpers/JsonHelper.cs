﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class JsonHelper
{
    class ArrayWrapper<T>
    {
        public T[] Items;
    }
    
    public static T[] FromJsonArray<T>(string json)
    {
        string objectWrapperJson = "{\"Items\": " + json + "}";

        var wrapper = JsonUtility.FromJson<ArrayWrapper<T>>(objectWrapperJson);

        return wrapper.Items;
    }
}
