﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "Configuration", menuName = "Game/Configuration")]
public class ConfigurationProvider : ScriptableObject
{
    [SerializeField]
    private string _apiUrl;

    public string ApiURL
    {
        get
        {
#if UNITY_EDITOR
            return "http://127.0.0.1:8080/api/";
#endif
            
#if UNITY_WEBGL
            return "/api/";
#endif

            return _apiUrl;
        }
    }
}
