using Entitas;

public class CommandFeature : Feature
{
    public CommandFeature(MoveCommandSystem moveCommandSystem, AttackCommandSystem attackCommandSystem) : base("Command")
    {
        Add(moveCommandSystem);
        Add(attackCommandSystem);
    }
}