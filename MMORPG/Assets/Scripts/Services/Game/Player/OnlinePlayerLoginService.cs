using System.Collections.Generic;
using Mirror;

public class OnlinePlayerLoginService : MessageHandlerService<ConnectMessage>, IPlayerLoginService
{
    private Contexts _contexts;
    private IServerMessageService _messageService;

    public OnlinePlayerLoginService(
        Contexts contexts,
        INetworkMessageService networkMessageService
    ) : base(networkMessageService)
    {
        _contexts = contexts;
    }

    protected override void HandleMessage(NetworkConnection connection, ConnectMessage message)
    {
        var playerEntity = _contexts.game.CreateEntity();
        playerEntity.AddNetworkIdentity((uint) playerEntity.creationIndex);
        playerEntity.AddPlayerNetwork(connection);
        // todo !!! creation index можно убрать player component и сделать login player component.
        playerEntity.AddPlayer(playerEntity.creationIndex, PlayerReadyState.Ready);
        playerEntity.AddReplication(0.0f);
    }
}