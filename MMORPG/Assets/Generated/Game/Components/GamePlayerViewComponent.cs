//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by Entitas.CodeGeneration.Plugins.ComponentContextApiGenerator.
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------
public partial class GameContext {

    public GameEntity playerViewEntity { get { return GetGroup(GameMatcher.PlayerView).GetSingleEntity(); } }
    public PlayerViewComponent playerView { get { return playerViewEntity.playerView; } }
    public bool hasPlayerView { get { return playerViewEntity != null; } }

    public GameEntity SetPlayerView(UnityEngine.GameObject newViewObject) {
        if (hasPlayerView) {
            throw new Entitas.EntitasException("Could not set PlayerView!\n" + this + " already has an entity with PlayerViewComponent!",
                "You should check if the context already has a playerViewEntity before setting it or use context.ReplacePlayerView().");
        }
        var entity = CreateEntity();
        entity.AddPlayerView(newViewObject);
        return entity;
    }

    public void ReplacePlayerView(UnityEngine.GameObject newViewObject) {
        var entity = playerViewEntity;
        if (entity == null) {
            entity = SetPlayerView(newViewObject);
        } else {
            entity.ReplacePlayerView(newViewObject);
        }
    }

    public void RemovePlayerView() {
        playerViewEntity.Destroy();
    }
}

//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by Entitas.CodeGeneration.Plugins.ComponentEntityApiGenerator.
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------
public partial class GameEntity {

    public PlayerViewComponent playerView { get { return (PlayerViewComponent)GetComponent(GameComponentsLookup.PlayerView); } }
    public bool hasPlayerView { get { return HasComponent(GameComponentsLookup.PlayerView); } }

    public void AddPlayerView(UnityEngine.GameObject newViewObject) {
        var index = GameComponentsLookup.PlayerView;
        var component = (PlayerViewComponent)CreateComponent(index, typeof(PlayerViewComponent));
        component.ViewObject = newViewObject;
        AddComponent(index, component);
    }

    public void ReplacePlayerView(UnityEngine.GameObject newViewObject) {
        var index = GameComponentsLookup.PlayerView;
        var component = (PlayerViewComponent)CreateComponent(index, typeof(PlayerViewComponent));
        component.ViewObject = newViewObject;
        ReplaceComponent(index, component);
    }

    public void RemovePlayerView() {
        RemoveComponent(GameComponentsLookup.PlayerView);
    }
}

//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by Entitas.CodeGeneration.Plugins.ComponentMatcherApiGenerator.
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------
public sealed partial class GameMatcher {

    static Entitas.IMatcher<GameEntity> _matcherPlayerView;

    public static Entitas.IMatcher<GameEntity> PlayerView {
        get {
            if (_matcherPlayerView == null) {
                var matcher = (Entitas.Matcher<GameEntity>)Entitas.Matcher<GameEntity>.AllOf(GameComponentsLookup.PlayerView);
                matcher.componentNames = GameComponentsLookup.componentNames;
                _matcherPlayerView = matcher;
            }

            return _matcherPlayerView;
        }
    }
}
