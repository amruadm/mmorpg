using Mirror;

public class PlayerDataMessage : MessageBase
{
    public int PlayerId;

    public override void Serialize(NetworkWriter writer)
    {
        writer.Write(PlayerId);
    }

    public override void Deserialize(NetworkReader reader)
    {
        PlayerId = reader.ReadInt32();
    }
}