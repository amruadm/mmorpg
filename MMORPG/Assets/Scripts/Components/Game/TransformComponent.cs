using Entitas;
using Entitas.CodeGeneration.Attributes;
using UnityEngine;

[Game, Pawn, Event(EventTarget.Self)]
public class TransformComponent : IComponent
{
    public Vector2 Position;
    public float Rotation;
}