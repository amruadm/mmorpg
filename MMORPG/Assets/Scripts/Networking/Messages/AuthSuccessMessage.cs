using Mirror;

public class AuthSuccessMessage : MessageBase
{
    public int UserId;

    public override void Serialize(NetworkWriter writer)
    {
        writer.Write(UserId);
    }

    public override void Deserialize(NetworkReader reader)
    {
        UserId = reader.ReadInt32();
    }
}