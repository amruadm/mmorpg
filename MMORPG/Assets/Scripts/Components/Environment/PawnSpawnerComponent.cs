using System;
using CodeGeneration.Attributes;
using Entitas;
using UnityEngine;

[PlacedEntity, Environment]
public class PawnSpawnerComponent : IComponent
{
    public int PawnId;
}
