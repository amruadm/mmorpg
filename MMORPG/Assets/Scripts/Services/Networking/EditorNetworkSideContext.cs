public class EditorNetworkSideContext : INetworkSideContext
{
    private bool _isEnabled;
    
    public EditorNetworkSideContext(bool isEnabled)
    {
        _isEnabled = isEnabled;
    }
    
    public bool IsServer => true;
    public bool IsClient => true;
    public bool IsEnabled => _isEnabled;
}