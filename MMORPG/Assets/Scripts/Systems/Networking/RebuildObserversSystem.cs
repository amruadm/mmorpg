using System.Collections.Generic;
using Entitas;
using Entitas.Unity;
using UnityEngine;

public class RebuildObserversSystem : IExecuteSystem
{
    private const int BufferAllocSize = 5000;
    
    private const float UpdateInterval = 1.0f;
    private const float ProximityDistance = 15.0f;

    private IGroup<PawnEntity> _group;
    private Contexts _contexts;
    private IServerMessageService _serverMessageService;

    private Collider[] _hitsBuffer3D = new Collider[BufferAllocSize];

    public RebuildObserversSystem(Contexts contexts, IServerMessageService serverMessageService)
    {
        _contexts = contexts;
        _serverMessageService = serverMessageService;
        _group = _contexts.pawn.GetGroup(PawnMatcher.AllOf(PawnMatcher.Pawn, PawnMatcher.NetworkIdentity, PawnMatcher.Proximity));
    }

    public void Execute()
    {
        foreach (var pawnEntity in _group.GetEntities())
        {
            pawnEntity.proximity.LastUpdateTime += Time.deltaTime;
            if (pawnEntity.proximity.LastUpdateTime >= UpdateInterval)
            {
                CalculateProximityFor(pawnEntity);

                pawnEntity.proximity.LastUpdateTime = 0.0f;
            }
        }
    }

    private void CalculateProximityFor(PawnEntity playerPawnEntity)
    {
        var oldObservers = new HashSet<uint>(playerPawnEntity.proximity.ObservedEntities);
        
        playerPawnEntity.proximity.ObservedEntities.Clear();
        
        var position = playerPawnEntity.movement.Agent.transform.position;

        var layers = 1 << LayerMask.NameToLayer("Pawn");

        int hitCount = Physics.OverlapSphereNonAlloc(position, ProximityDistance, _hitsBuffer3D, layers);
        
        for (int i = 0; i < hitCount; i++)
        {
            Collider hit = _hitsBuffer3D[i];
            
            var entityLink = hit.gameObject.GetEntityLink();
            if (entityLink)
            {
                if (entityLink.entity is PawnEntity)
                {
                    var pawnEntity = entityLink.entity as PawnEntity;
                    if (pawnEntity.hasNetworkIdentity)
                    {
                        playerPawnEntity.proximity.ObservedEntities.Add(pawnEntity.networkIdentity.Id);
                        if (playerPawnEntity.hasPlayerOwner && !oldObservers.Contains(pawnEntity.networkIdentity.Id))
                        {
                            var player = _contexts.game.GetEntityWithPlayer(playerPawnEntity.playerOwner.PlayerId);
                            if (null != player)
                            {
                                _serverMessageService.Send(player.playerNetwork.Connection, new EntityObserveEnterMessage
                                {
                                    Id = pawnEntity.networkIdentity.Id,
                                    PawnId = pawnEntity.pawn.Id,
                                });                                
                            }
                        }
                    }
                }
            }
        }

        foreach (var observer in oldObservers)
        {
            if (playerPawnEntity.hasPlayerOwner && !playerPawnEntity.proximity.ObservedEntities.Contains(observer))
            {
                var player = _contexts.game.GetEntityWithPlayer(playerPawnEntity.playerOwner.PlayerId);
                if (null != player)
                {
                    _serverMessageService.Send(player.playerNetwork.Connection, new EntityObserveLeaveMessage
                    {
                        Id = playerPawnEntity.networkIdentity.Id
                    });                                
                }
            }
        }
    }
}