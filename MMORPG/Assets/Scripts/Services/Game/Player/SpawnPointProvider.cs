using UnityEngine;

public class SpawnPointProvider
{
    private GameObject _spawnPoint;
    
    public SpawnPointProvider()
    {
        _spawnPoint = GameObject.FindWithTag("Respawn");
    }

    public GameObject GetSpawnPoint()
    {
        return _spawnPoint;
    }
}