using Mirror;


public class AuthMessage : MessageBase
{
    public string Login;
    public string Password;

    public override void Serialize(NetworkWriter writer)
    {
        writer.Write(Login);
        writer.Write(Password);
    }

    public override void Deserialize(NetworkReader reader)
    {
        Password = reader.ReadString();
        Login = reader.ReadString();
    }
}