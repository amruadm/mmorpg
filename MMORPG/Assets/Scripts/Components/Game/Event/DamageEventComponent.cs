using Entitas;
using Entitas.CodeGeneration.Attributes;

[Game, Event(EventTarget.Self)]
public class DamageEventComponent : IComponent
{
    public PawnEntity Causer;
    public PawnEntity Target;

    public float Damage;
}