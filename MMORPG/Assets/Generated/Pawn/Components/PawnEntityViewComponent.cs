//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by Entitas.CodeGeneration.Plugins.ComponentEntityApiGenerator.
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------
public partial class PawnEntity {

    public EntityViewComponent entityView { get { return (EntityViewComponent)GetComponent(PawnComponentsLookup.EntityView); } }
    public bool hasEntityView { get { return HasComponent(PawnComponentsLookup.EntityView); } }

    public void AddEntityView(UnityEngine.GameObject newViewObject) {
        var index = PawnComponentsLookup.EntityView;
        var component = (EntityViewComponent)CreateComponent(index, typeof(EntityViewComponent));
        component.ViewObject = newViewObject;
        AddComponent(index, component);
    }

    public void ReplaceEntityView(UnityEngine.GameObject newViewObject) {
        var index = PawnComponentsLookup.EntityView;
        var component = (EntityViewComponent)CreateComponent(index, typeof(EntityViewComponent));
        component.ViewObject = newViewObject;
        ReplaceComponent(index, component);
    }

    public void RemoveEntityView() {
        RemoveComponent(PawnComponentsLookup.EntityView);
    }
}

//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by Entitas.CodeGeneration.Plugins.ComponentEntityApiInterfaceGenerator.
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------
public partial class PawnEntity : IEntityViewEntity { }

//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by Entitas.CodeGeneration.Plugins.ComponentMatcherApiGenerator.
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------
public sealed partial class PawnMatcher {

    static Entitas.IMatcher<PawnEntity> _matcherEntityView;

    public static Entitas.IMatcher<PawnEntity> EntityView {
        get {
            if (_matcherEntityView == null) {
                var matcher = (Entitas.Matcher<PawnEntity>)Entitas.Matcher<PawnEntity>.AllOf(PawnComponentsLookup.EntityView);
                matcher.componentNames = PawnComponentsLookup.componentNames;
                _matcherEntityView = matcher;
            }

            return _matcherEntityView;
        }
    }
}
