public interface INetworkSideContext
{
    bool IsServer { get; }
    bool IsClient { get; }
    bool IsEnabled { get; }
}