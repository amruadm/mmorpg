using Entitas;

public class ViewFeature : Feature
{
    public ViewFeature(
        SpawnPawnViewSystem spawnPawnViewSystem,
        PawnViewSystem pawnViewSystem,
        SpawnPlayerViewSystem spawnPlayerViewSystem,
        PlayerViewSystem playerViewSystem
    ) : base("View")
    {
        Add(spawnPawnViewSystem);
        Add(pawnViewSystem);
        Add(spawnPlayerViewSystem);
        Add(playerViewSystem);
    }
}