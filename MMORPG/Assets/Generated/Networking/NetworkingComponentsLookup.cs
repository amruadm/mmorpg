//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by Entitas.CodeGeneration.Plugins.ComponentLookupGenerator.
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------
public static class NetworkingComponentsLookup {

    public const int MoveCommandMessage = 0;
    public const int NetworkConnection = 1;
    public const int NetworkIdentity = 2;
    public const int Replication = 3;
    public const int SyncTransform = 4;

    public const int TotalComponents = 5;

    public static readonly string[] componentNames = {
        "MoveCommandMessage",
        "NetworkConnection",
        "NetworkIdentity",
        "Replication",
        "SyncTransform"
    };

    public static readonly System.Type[] componentTypes = {
        typeof(MoveCommandMessage),
        typeof(NetworkConnectionComponent),
        typeof(NetworkIdentityComponent),
        typeof(ReplicationComponent),
        typeof(SyncTransformComponent)
    };
}
