using Mirror;
using UnityEngine;
using Zenject;

public class SinglePlayerLoginService : MonoBehaviour, IPlayerLoginService
{
    private Contexts _contexts;
    private IClientMessageService _clientMessageService;
    
    [Inject]
    public void Construct(Contexts contexts, IClientMessageService messageService)
    {
        _contexts = contexts;
        _clientMessageService = messageService;
        _clientMessageService.RegisterHandler<ConnectMessage>(ConnectMessageHandler);
    }

    private void ConnectMessageHandler(NetworkConnection arg1, ConnectMessage arg2)
    {
        Debug.Log("ConnectMessageHandler");
    }

    protected void Start()
    {
        var playerEntity =  _contexts.game.CreateEntity();
        playerEntity.AddPlayer(playerEntity.creationIndex, PlayerReadyState.Ready);
        playerEntity.AddLocalPlayer(playerEntity.player);
    }
}