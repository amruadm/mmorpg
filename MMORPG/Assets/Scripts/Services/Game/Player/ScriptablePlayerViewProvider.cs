using UnityEngine;

[CreateAssetMenu(menuName = "Game / Player", fileName = "Player View")]
public class ScriptablePlayerViewProvider : ScriptableObject, IPlayerViewProvider
{
    [SerializeField]
    private GameObject _prefab;
    public GameObject PlayerPrefab => _prefab;
}