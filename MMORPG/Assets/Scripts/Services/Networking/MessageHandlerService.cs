using Mirror;
using UnityEngine;

public interface IMessageHandlerService
{
}

public abstract class MessageHandlerService<T> : IMessageHandlerService where T : IMessageBase, new()
{
    public MessageHandlerService(INetworkMessageService networkMessageService)
    {
        networkMessageService.RegisterHandler<T>(HandleMessage);
    }

    protected abstract void HandleMessage(NetworkConnection connection, T message);
}