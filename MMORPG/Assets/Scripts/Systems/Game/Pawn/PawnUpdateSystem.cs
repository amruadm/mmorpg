using Entitas;
using UnityEngine;

public class PawnUpdateSystem : IExecuteSystem
{
    private IGroup<PawnEntity> _group;
    
    public PawnUpdateSystem(Contexts contexts)
    {
        _group = contexts.pawn.GetGroup(PawnMatcher.Pawn);
    }

    public void Execute()
    {
        foreach (var pawnEntity in _group.GetEntities())
        {
            if (!pawnEntity.movement.Agent.isStopped)
            {
                var pos = new Vector2();
                pos.x = pawnEntity.movement.Agent.transform.position.x;
                pos.y = pawnEntity.movement.Agent.transform.position.z;

                float rotation = pawnEntity.movement.Agent.transform.rotation.eulerAngles.y;

                pawnEntity.ReplaceTransform(pos, rotation);                
            }
        }
    }
}