using Entitas;
using UnityEngine;

public class AttackCommandSystem : ICleanupSystem
{
    private Contexts _contexts;
    private IGroup<CommandEntity> _group;
    private IPawnAttributeValueProvider _pawnAttributeValueProvider;

    public AttackCommandSystem(Contexts contexts, IPawnAttributeValueProvider pawnAttributeValueProvider)
    {
        _contexts = contexts;
        _group = contexts.command.GetGroup(CommandMatcher.AttackCommand);
        _pawnAttributeValueProvider = pawnAttributeValueProvider;
    }
    
    public void Cleanup()
    {
        foreach (var commandEntity in _group.GetEntities())
        {
            var instigtorPawnEntity = _contexts.pawn.GetEntityWithNetworkIdentity(commandEntity.attackCommand.InstigatorPawn);
            var targetPawnEntity = _contexts.pawn.GetEntityWithNetworkIdentity(commandEntity.attackCommand.TargetPawn);
            if (null != targetPawnEntity && null != instigtorPawnEntity)
            {
                if (instigtorPawnEntity.isDie)
                {
                    commandEntity.Destroy();

                    return;
                }
                
                if (instigtorPawnEntity.hasAttack)
                    instigtorPawnEntity.RemoveAttack();
                
                float attackDistance = _pawnAttributeValueProvider.GetAttackDistance(instigtorPawnEntity);

                float distance = Vector2.Distance(instigtorPawnEntity.transform.Position, targetPawnEntity.transform.Position);
                if (distance > attackDistance)
                {
                    instigtorPawnEntity.movement.Agent.SetDestination(targetPawnEntity.movement.Agent.transform.position);
                }
                else
                {
                    instigtorPawnEntity.movement.Agent.SetDestination(instigtorPawnEntity.movement.Agent.transform.position);
                    
                    instigtorPawnEntity.AddAttack((int) commandEntity.attackCommand.TargetPawn, 0.0f, false);
                    
                    commandEntity.Destroy();
                }
            }
            else
            {
                commandEntity.Destroy();
            }
        }
    }
}