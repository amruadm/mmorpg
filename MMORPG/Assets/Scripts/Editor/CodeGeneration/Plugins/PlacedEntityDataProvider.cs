using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using CodeGeneration.Attributes;
using DesperateDevs.CodeGeneration;
using DesperateDevs.CodeGeneration.Plugins;
using DesperateDevs.Serialization;
using DesperateDevs.Utils;
using Entitas.CodeGeneration.Attributes;
using Entitas.CodeGeneration.Plugins;

namespace CodeGeneration.Plugins
{
    public class PlacedEntityDataProvider : IDataProvider, IConfigurable, ICachable
    {
        public string name => "PlacedEntity";
        public int priority => 0;
        public bool runInDryMode => true;
        public CodeGeneratorData[] GetData()
        {
            return Assembly.GetAssembly(typeof(PlacedEntityAttribute))
                .GetTypes()
                .Where(t => null != t.GetCustomAttribute<PlacedEntityAttribute>() && null != t.GetCustomAttribute<ContextAttribute>())
                .Select(t => new PlacedEntityData
                    {
                        Name = t.Name,
                        MemberData = GetTypeData(t),
                        ContextAttribute = t.GetCustomAttribute<ContextAttribute>()
                    })
                .ToArray()
            ;
        }

        private MemberData[] GetTypeData(Type type)
        {
            return type.GetFields()
                .Select(field => new MemberData(field.FieldType.ToCompilableString(), field.Name))
                .ToArray()
            ;
        }

        public void Configure(Preferences preferences)
        {
            _projectPathConfig.Configure(preferences);
        }

        public Dictionary<string, string> defaultProperties => _projectPathConfig.defaultProperties;
        public Dictionary<string, object> objectCache { get; set; }
        private readonly ProjectPathConfig _projectPathConfig = new ProjectPathConfig();
    }
}