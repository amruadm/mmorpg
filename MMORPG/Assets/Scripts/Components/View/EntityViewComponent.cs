using Entitas;
using UnityEngine;

[Game, Pawn]
public class EntityViewComponent : IComponent
{
    public GameObject ViewObject;
}