using Mirror;

[ServerSide]
public class AttackCommandMessageHandler : MessageHandlerService<AttackCommandMessage>
{
    private Contexts _contexts;
    
    public AttackCommandMessageHandler(Contexts contexts, IServerMessageService networkMessageService) : base(networkMessageService)
    {
        _contexts = contexts;
    }

    protected override void HandleMessage(NetworkConnection connection, AttackCommandMessage message)
    {
        var playerEntity = _contexts.game.GetEntityWithPlayerNetwork(connection);
        if (null == playerEntity)
            return;

        var pawn = _contexts.pawn.GetEntityWithPlayerOwner(playerEntity.player.Id);
        if (null == pawn)
            return;

        if (pawn.networkIdentity.Id == message.TargetId)
            return;
        
        CommandHelper.ClearCommands(_contexts.command, pawn.networkIdentity.Id);

        var command = _contexts.command.CreateEntity();
        command.AddAttackCommand(pawn.networkIdentity.Id, (uint) message.TargetId);
    }
}