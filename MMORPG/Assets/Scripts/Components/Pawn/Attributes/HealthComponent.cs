using Entitas;

[Pawn]
public class HealthComponent : IComponent, IAttribute
{
    public float Value;
}