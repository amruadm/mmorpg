﻿using System.Collections;
using System.Collections.Generic;
using Entitas;
using UnityEngine;
using Zenject;

public class FeautureController : MonoBehaviour
{
    private Systems _systems;

    [Inject]
    public void Construct(Feature[] features)
    {
        _systems = new Systems();

        foreach (var feature in features)
        {
            _systems.Add(feature);
        }
    }

    void Start()
    {
        _systems.Initialize();
    }

    void Update()
    {
        _systems.Execute();
        _systems.Cleanup();
    }
}