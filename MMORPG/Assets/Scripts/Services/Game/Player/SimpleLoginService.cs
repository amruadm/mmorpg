using Mirror;
using UnityEngine;

public class SimpleLoginService : MessageHandlerService<ConnectMessage>, IPlayerLoginService
{
    private Contexts _contexts;
    
    public SimpleLoginService(Contexts contexts, IServerMessageService networkMessageService) : base(networkMessageService)
    {
        _contexts = contexts;
        
        networkMessageService.RegisterHandler<DisconnectMessage>(HandleDisconnectMessage);
    }

    private void HandleDisconnectMessage(NetworkConnection connection, DisconnectMessage message)
    {
        var entity = _contexts.game.GetEntityWithPlayerNetwork(connection);
        if (null != entity)
        {
            entity.Destroy();
        }
    }

    protected override void HandleMessage(NetworkConnection connection, ConnectMessage message)
    {
        var playerEntity =  _contexts.game.CreateEntity();
        playerEntity.AddPlayer(playerEntity.creationIndex, PlayerReadyState.Ready);
        playerEntity.AddPlayerNetwork(connection);
        playerEntity.AddReplication(0.0f);
    }
}