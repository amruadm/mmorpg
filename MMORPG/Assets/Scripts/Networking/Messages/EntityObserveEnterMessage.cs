using Mirror;

public class EntityObserveEnterMessage : IdentityMessage
{
    public int PawnId;

    public override void Serialize(NetworkWriter writer)
    {
        base.Serialize(writer);

        writer.Write(PawnId);
    }

    public override void Deserialize(NetworkReader reader)
    {
        base.Deserialize(reader);

        PawnId = reader.ReadInt32();
    }
}