using System.Collections.Generic;
using Entitas;
using UnityEngine;

public class SpawnPawnViewSystem : ReactiveSystem<PawnEntity>
{
    private IPawnFactory _pawnFactory;
    private IPawnDataProvider _pawnDataProvider;
    
    public SpawnPawnViewSystem(Contexts contexts, IPawnFactory pawnFactory, IPawnDataProvider pawnDataProvider) : base(contexts.pawn)
    {
        _pawnFactory = pawnFactory;
        _pawnDataProvider = pawnDataProvider;
    }

    protected override ICollector<PawnEntity> GetTrigger(IContext<PawnEntity> context)
    {
        return context.CreateCollector(PawnMatcher.Pawn.Added());
    }

    protected override bool Filter(PawnEntity entity)
    {
        return entity.hasPawn;
    }

    protected override void Execute(List<PawnEntity> entities)
    {
        foreach (var pawnEntity in entities)
        {
            var view = _pawnFactory.CreatePawnViewFromData(_pawnDataProvider.GetAllData()[PlayerSpawnSystem.PlayerPawnPrefabId]);

            pawnEntity.AddEntityView(view);
        }
    }
}