using Entitas;

public interface IDamageService
{
    float CalcDamage(IEntity damageSource);
}