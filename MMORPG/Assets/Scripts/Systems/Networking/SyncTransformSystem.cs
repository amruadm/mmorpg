using Entitas;
using UnityEngine;

public class SyncTransformSystem : IExecuteSystem
{
    const float PredictionValue = 0.5f;
    
    private IGroup<PawnEntity> _group;
    
    public SyncTransformSystem(Contexts contexts)
    {
        _group = contexts.pawn.GetGroup(PawnMatcher.SyncTransform);
    }
    
    public void Execute()
    {
        foreach (var pawnEntity in _group.GetEntities())
        {
            float syncDeltaTime = Time.time - pawnEntity.syncTransform.LastSyncTime;

            float a = Mathf.Clamp01(syncDeltaTime * PredictionValue);

            var position = Vector2.Lerp(pawnEntity.transform.Position, pawnEntity.syncTransform.Position, a);

            var rotation = Mathf.Lerp(pawnEntity.transform.Rotation, pawnEntity.syncTransform.Rotation, a);
            
            pawnEntity.ReplaceTransform(position, rotation);
        }
    }
}