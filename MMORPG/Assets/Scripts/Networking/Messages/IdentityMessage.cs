using Mirror;

public abstract class IdentityMessage : MessageBase
{
    public uint Id;

    public override void Serialize(NetworkWriter writer)
    {
        writer.Write(Id);
    }

    public override void Deserialize(NetworkReader reader)
    {
        Id = reader.ReadUInt32();
    }
}