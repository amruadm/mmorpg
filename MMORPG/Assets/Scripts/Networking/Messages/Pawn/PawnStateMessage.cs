using Mirror;
using UnityEngine;

public class PawnStateMessage : IdentityMessage
{
    public int PawnId;
    public float Health;
    public Vector2 Position;
    public Vector2 Velocity;
    public float Rotation;
    public int PlayerId;

    public override void Serialize(NetworkWriter writer)
    {
        base.Serialize(writer);
        
        writer.Write(PawnId);
        writer.Write(Health);
        writer.Write(Position);
        writer.Write(Velocity);
        writer.Write(Rotation);
        writer.Write(PlayerId);
    }

    public override void Deserialize(NetworkReader reader)
    {
        base.Deserialize(reader);

        PawnId = reader.ReadInt32();
        Health = reader.ReadSingle();
        Position = reader.ReadVector2();
        Velocity = reader.ReadVector2();
        Rotation = reader.ReadSingle();
        PlayerId = reader.ReadInt32();
    }
}