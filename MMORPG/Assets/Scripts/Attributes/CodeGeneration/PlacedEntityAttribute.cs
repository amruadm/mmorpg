using System;

namespace CodeGeneration.Attributes
{
    [AttributeUsage(AttributeTargets.Class)]
    public class PlacedEntityAttribute : Attribute
    {
    }
}