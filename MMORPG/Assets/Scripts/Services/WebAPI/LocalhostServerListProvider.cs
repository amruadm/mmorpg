public class LocalhostServerListProvider : IServerListProvider
{
    public void Load(ServerListLoadedHandler onServerListLoaded)
    {
        onServerListLoaded?.Invoke(new ServerInfo[]
        {
            new ServerInfo
            {
                addr = "127.0.0.1",
                name = "localhost",
                port = 7777,
            }, 
        });
    }
}