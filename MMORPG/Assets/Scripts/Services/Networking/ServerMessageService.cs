using System;
using Mirror;
using UnityEngine;

public interface IServerMessageService : INetworkMessageService
{
    void Send<T>(NetworkConnection connection, T message) where T : IMessageBase, new();
}

public class ServerMessageService : IServerMessageService
{
    public void RegisterHandler<T>(Action<NetworkConnection, T> handler) where T : IMessageBase, new()
    {
        NetworkServer.RegisterHandler(handler);
    }

    public void UnregisterHandler<T>() where T : IMessageBase
    {
        NetworkServer.UnregisterHandler<T>();
    }

    public void Send<T>(NetworkConnection connection, T message) where T : IMessageBase, new()
    {
        NetworkServer.SendToClient(connection.connectionId, message);
    }
}