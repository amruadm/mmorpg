using System.Collections.Generic;
using Entitas;
using Entitas.Unity;
using UnityEngine;
using UnityEngine.AI;

public class PawnFromPrefabFactory : IPawnFactory
{
    private Contexts _contexts;
    private RandomValueProvider _randomValueProvider;
    private IPawnDataProvider _pawnDataProvider;
    private INetworkSideContext _networkSideContext;
    
    public PawnFromPrefabFactory(
        Contexts contexts,
        RandomValueProvider randomValueProvider,
        IPawnDataProvider pawnDataProvider,
        INetworkSideContext networkSideContext
    ) {
        _contexts = contexts;
        _randomValueProvider = randomValueProvider;
        _pawnDataProvider = pawnDataProvider;
        _networkSideContext = networkSideContext;
    }
    
    public PawnEntity CreateEmptyPawn()
    {
        GameObject go = new GameObject("Pawn");
        var navMeshAgent = go.AddComponent<NavMeshAgent>();
        
        var pawnEntity = _contexts.pawn.CreateEntity();
        pawnEntity.AddPawn(0);
        pawnEntity.AddMovement(navMeshAgent);

        PreparePawn(pawnEntity);
        
        return pawnEntity;
    }

    public PawnEntity CreatePawnFromData(IPawnData pawnData)
    {
        var pawnEntity = _contexts.pawn.CreateEntity();
        
        if (_networkSideContext.IsServer)
        {
            var createdObject = GameObject.Instantiate(pawnData.Prefab);
            pawnEntity.AddPawn(pawnData.Id);
            pawnEntity.AddMovement(createdObject.GetComponent<NavMeshAgent>());
            createdObject.Link(pawnEntity);
        }
        else
        {
            pawnEntity.AddPawn(pawnData.Id);
        }

        PreparePawn(pawnEntity);

        return pawnEntity;
    }

    public PawnEntity CreatePawnFromId(int id)
    {
        var allData = _pawnDataProvider.GetAllData();

        if (!allData.ContainsKey(id))
        {
            return null;
        }

        return CreatePawnFromData(allData[id]);
    }

    public GameObject CreatePawnViewFromData(IPawnData pawnData)
    {
        return GameObject.Instantiate(pawnData.ViewPrefab);
    }

    private void PreparePawn(PawnEntity entity)
    {
        entity.AddTransform(Vector3.zero, 0.0f);
        entity.AddHealth(100.0f);
        entity.AddDamage(5.0f, 10.0f);
        entity.AddRandomSeed(_randomValueProvider.RandomSeed);

        if (_networkSideContext.IsServer)
        {
            entity.AddNetworkIdentity((uint) entity.creationIndex);
            entity.AddProximity(0.0f, new HashSet<uint>());
            entity.AddReplication(0.0f);
        }
    }
}