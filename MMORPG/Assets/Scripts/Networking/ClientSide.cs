﻿using System;
using System.Collections;
using System.Collections.Generic;
using Mirror;
using Mirror.Websocket;
using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.Serialization;
using Zenject;

public class ClientSide : MonoBehaviour
{
    private MMORPGNetworkManager _networkManager;
    private IServerListProvider _serverListProvider;
    
    [Inject]
    public void Construct(MMORPGNetworkManager networkManager, IServerListProvider serverListProvider)
    {
        _networkManager = networkManager;
        _serverListProvider = serverListProvider;
    }
    
    void Start()
    {
        _serverListProvider.Load(OnListLoaded);
    }

    private void OnListLoaded(ServerInfo[] servers)
    {
        if (servers.Length > 0)
        {
            _networkManager.ServerAddr = servers[0].addr;
            _networkManager.ServerPort = servers[0].port;
            
            Debug.Log("Connecting to server: " + servers[0].addr + ":" + servers[0].port);
            
            _networkManager.StartClient();            
        }
    }
}