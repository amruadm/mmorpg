using Entitas;
using Entitas.CodeGeneration.Attributes;
using UnityEngine;

[Input, Unique]
public class LeftClickComponent : IComponent
{
    public Vector2 ScreenPosition;
}