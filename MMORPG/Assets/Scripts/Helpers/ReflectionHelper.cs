using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;

public static class ReflectionHelper
{
    public static ICollection<Type> GetImplementations(Type type, Assembly assembly = null)
    {
        if (assembly == null)
        {
            assembly = Assembly.GetAssembly(typeof(ReflectionHelper));
        }
        
        return assembly
            .GetTypes()
            .Where(t => t.IsClass && !t.IsAbstract && t.GetInterfaces().Contains(type))
            .ToArray()
        ;
    }

    public static ICollection<Type> GetChildClasses(Type type, Assembly assembly = null)
    {
        if (assembly == null)
        {
            assembly = Assembly.GetAssembly(typeof(ReflectionHelper));
        }
        
        return assembly
            .GetTypes()
            .Where(t => t.IsClass && !t.IsAbstract && t.IsSubclassOf(type))
            .ToArray()
        ;
    }
}