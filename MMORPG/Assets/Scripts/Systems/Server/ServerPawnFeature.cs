public class ServerPawnFeature : Feature
{
    public ServerPawnFeature(
        PawnUpdateSystem pawnUpdateSystem,
        AttackSystem attackSystem
    ) : base("Pawn")
    {
        Add(pawnUpdateSystem);
        Add(attackSystem);
    }
}