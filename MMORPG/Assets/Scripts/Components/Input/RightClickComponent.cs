using Entitas;
using Entitas.CodeGeneration.Attributes;
using UnityEngine;

[Input, Unique]
public class RightClickComponent : IComponent
{
    public Vector2 ScreenPosition;
}