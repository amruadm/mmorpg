using Entitas;
using Mirror;
using UnityEngine;

[Networking]
public class MoveCommandMessage : MessageBase, IComponent
{
    public Vector2 TargetPosition;

    public override void Serialize(NetworkWriter writer)
    {
        writer.Write(TargetPosition);
    }

    public override void Deserialize(NetworkReader reader)
    {
        TargetPosition = reader.ReadVector2();
    }
}