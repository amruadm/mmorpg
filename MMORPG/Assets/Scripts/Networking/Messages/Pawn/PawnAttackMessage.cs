using Mirror;

public class PawnAttackMessage : MessageBase
{
    public int CauserId;
    public int TargetId;

    public override void Serialize(NetworkWriter writer)
    {
        writer.Write(CauserId);
        writer.Write(TargetId);
    }

    public override void Deserialize(NetworkReader reader)
    {
        CauserId = reader.ReadInt32();
        TargetId = reader.ReadInt32();
    }
}