using Entitas;

[Pawn]
public class AttackComponent : IComponent
{
    public int TargetId;
    public float Timer;
    public bool Triggered;
}