using Entitas;
using UnityEngine;

[Environment]
public class GameObjectLinkComponent : IComponent
{
    public GameObject Value;
}