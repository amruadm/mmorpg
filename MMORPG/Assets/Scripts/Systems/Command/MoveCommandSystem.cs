using Entitas;
using UnityEngine;

public class MoveCommandSystem : ICleanupSystem
{
    private Contexts _contexts;
    private IGroup<CommandEntity> _group;
    
    public MoveCommandSystem(Contexts contexts)
    {
        _group = contexts.command.GetGroup(CommandMatcher.MoveCommand);
        _contexts = contexts;
    }

    public void Cleanup()
    {
        foreach (var commandEntity in _group.GetEntities())
        {
            var pawn = _contexts.pawn.GetEntityWithNetworkIdentity(commandEntity.moveCommand.PawnId);
            if (null != pawn && !pawn.isDie)
            {
                if (pawn.hasAttack)
                    pawn.RemoveAttack();
                
                var pos2d = commandEntity.moveCommand.TargetPosition;
                var target = new Vector3(pos2d.x, 0.0f, pos2d.y);
                
                pawn.movement.Agent.SetDestination(target);                
            }
            
            commandEntity.Destroy();
        }
    }
}