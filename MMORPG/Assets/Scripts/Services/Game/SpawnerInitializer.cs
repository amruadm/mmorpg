using System.Collections;
using UnityEngine;
using Zenject;

public class SpawnerInitializer : MonoBehaviour
{
    private Contexts _contexts;
    
    [Inject]
    public void Initialize(Contexts contexts)
    {
        _contexts = contexts;
    }
    
    void Start()
    {
        StartCoroutine(DelayedSpawn());
    }

    private IEnumerator DelayedSpawn()
    {
        yield return new WaitForSeconds(1.0f);
        
        var spawners = FindObjectsOfType<PawnSpawnerScript>();
        foreach (var spawner in spawners)
        {
            var spawnerEntity = _contexts.environment.CreateEntity();
            spawner.Initialize(spawnerEntity);
            spawnerEntity.AddGameObjectLink(spawner.gameObject);
        }
    }
}
