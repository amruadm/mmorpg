//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by Entitas.CodeGeneration.Plugins.ComponentEntityApiGenerator.
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------
public partial class GameEntity {

    public RandomSeedComponent randomSeed { get { return (RandomSeedComponent)GetComponent(GameComponentsLookup.RandomSeed); } }
    public bool hasRandomSeed { get { return HasComponent(GameComponentsLookup.RandomSeed); } }

    public void AddRandomSeed(int newValue) {
        var index = GameComponentsLookup.RandomSeed;
        var component = (RandomSeedComponent)CreateComponent(index, typeof(RandomSeedComponent));
        component.Value = newValue;
        AddComponent(index, component);
    }

    public void ReplaceRandomSeed(int newValue) {
        var index = GameComponentsLookup.RandomSeed;
        var component = (RandomSeedComponent)CreateComponent(index, typeof(RandomSeedComponent));
        component.Value = newValue;
        ReplaceComponent(index, component);
    }

    public void RemoveRandomSeed() {
        RemoveComponent(GameComponentsLookup.RandomSeed);
    }
}

//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by Entitas.CodeGeneration.Plugins.ComponentEntityApiInterfaceGenerator.
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------
public partial class GameEntity : IRandomSeedEntity { }

//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by Entitas.CodeGeneration.Plugins.ComponentMatcherApiGenerator.
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------
public sealed partial class GameMatcher {

    static Entitas.IMatcher<GameEntity> _matcherRandomSeed;

    public static Entitas.IMatcher<GameEntity> RandomSeed {
        get {
            if (_matcherRandomSeed == null) {
                var matcher = (Entitas.Matcher<GameEntity>)Entitas.Matcher<GameEntity>.AllOf(GameComponentsLookup.RandomSeed);
                matcher.componentNames = GameComponentsLookup.componentNames;
                _matcherRandomSeed = matcher;
            }

            return _matcherRandomSeed;
        }
    }
}
