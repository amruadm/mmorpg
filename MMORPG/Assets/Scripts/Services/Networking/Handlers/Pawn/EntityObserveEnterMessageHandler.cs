using Mirror;

[ClientSide]
public class EntityObserveEnterMessageHandler : MessageHandlerService<EntityObserveEnterMessage>
{
    private Contexts _contexts;
    private IPawnFactory _pawnFactory;

    public EntityObserveEnterMessageHandler(
        Contexts contexts,
        IClientMessageService networkMessageService,
        IPawnFactory pawnFactory
    ) : base(networkMessageService)
    {
        _contexts = contexts;
        _pawnFactory = pawnFactory;
    }

    protected override void HandleMessage(NetworkConnection connection, EntityObserveEnterMessage message)
    {
        PawnEntity pawnEntity = _contexts.pawn.GetEntityWithNetworkIdentity(message.Id);
        if (null == pawnEntity)
        {
            pawnEntity = _pawnFactory.CreatePawnFromId(message.PawnId);
            pawnEntity.AddNetworkIdentity(message.Id);
        }
    }
}