using System.Collections.Generic;
using Entitas;

public class InputFeature : Feature
{
    public InputFeature(
        InputSystem inputSystem,
        MoveCommandInputSystem moveCommandInputSystem,
        AttackCommandInputSystem attackCommandInputSystem
    ) : base("Input")
    {
        Add(inputSystem);
        Add(moveCommandInputSystem);
        Add(attackCommandInputSystem);
    }
}